package com.diffblue;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerRequest;

public class VertxXss extends AbstractVerticle implements Handler<HttpServerRequest> {

  @Override
  public void start(Future<Void> fut) {
    vertx
	    .createHttpServer()
	    .requestHandler(this)
	    .listen(8080, new ListenHandler(fut));
  }

  public void handle(HttpServerRequest request) {
    request.response().end("User-Agent was " + request.getHeader("User-Agent"));
  }

}

class ListenHandler implements Handler<AsyncResult<HttpServer>> {

  private Future fut;

  public ListenHandler(Future fut) {
    this.fut = fut;
  }

  public void handle(AsyncResult<HttpServer> result) {
    if (result.succeeded()) {
      fut.complete();
    } else {
      fut.fail(result.cause());
    }
  }

}
